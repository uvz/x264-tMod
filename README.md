### Notes

There are few minor differences compared to the jpsdr builds:
- audio is disabled;
- not applied patches:
    * AviSynth 16-bit hack (AviSynth+ native high bit depth is officiallly long time supported);
    * f3kdb usage for converting from higher bit depth to output bit depth;
    * double unicode buffer;
    * weightp 2 for Blu-ray;
    * rbsp_alignment_zero_bit;
    * filters: hqdn3d, pad, vflip, yadif;
    * video filter subtitles;
    * fix yadif after r2984;
    * add missing audio directories.
- reverted patches:
    * AVI support.

### Building

[**Here**](https://gitlab.com/uvz/x264-tMod/-/snippets/1982897).
